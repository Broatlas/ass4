/****************************************************************************************
* Filename: Ellipse.cpp
* Version: 1.0
* Author: Robert Taracha
* Student No: 040820843
* Course Name/Number: C++ CST8219
* Lab Sect: 302
* Assignment #: 3
* Assignment Name: Vector Graphic
* Prof. Andrew Tyler
* Due Date : 12/2/16
* Submitted : 12/2/16
* Header Files:  Ellipse.h, iostream
* Purpose: Defines output for Line classs
*****************************************************************************************/
#include <iostream>
#include "Ellipse.h"

using namespace std;	
/****************************************************************************************
* Name: CalculateCentre()
* Purpose: to return the centre coordinates of the ellipse shape.
* Function In parameters: none
* Function Out Parameters: returns Pair
* Version: 1.0
* Author: Robert Taracha
****************************************************************************************/
Pair Ellipse::CalculateCentre(){
	//divide by one to activate the overloaded operator.
	return Pair((this->p1/double(1)));
}
/****************************************************************************************
* Name: Report()
* Purpose: to output the type of shape and coordinates inside the Ellipse object.
* Function In parameters: none
* Function Out Parameters: returns void
* Version: 1.0
* Author: Robert Taracha
****************************************************************************************/
void Ellipse::Report(){

	cout << "Shape ELLIPSE " << this->name << endl;
	cout << "centre coordinates: ";
	this->p1.Report();
	cout << "axes dimensions: ";
	this->p2.Report();
}