// Line.h
#ifndef LINE_H_
#define LINE_H_
#include "Shape.h"
using namespace std;
class Line :public Shape
{
public:
	Line(char* name, Pair start, Pair end) :Shape(name, start, end){};
	~Line(){}
	Pair CalculateCentre();
	void Report();
};
#endif