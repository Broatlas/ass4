/****************************************************************************************
* Filename: Line.cpp
* Version: 1.0
* Author: Robert Taracha
* Student No: 040820843
* Course Name/Number: C++ CST8219
* Lab Sect: 302
* Assignment #: 3
* Assignment Name: Vector Graphic
* Prof. Andrew Tyler
* Due Date : 12/2/16
* Submitted : 12/2/16
* Header Files:  Line.h, iostream
* Purpose: Defines output for Line classs
*****************************************************************************************/
#include <iostream>
#include "Line.h"

using namespace std;
/****************************************************************************************
* Name: CalculateCentre()
* Purpose: to return the centre coordinates of the line shape.
* Function In parameters: none
* Function Out Parameters: returns Pair
* Version: 1.0
* Author: Robert Taracha
****************************************************************************************/
Pair Line::CalculateCentre(){
	//using defined operators in pair.h
	return Pair(((this->p1 + this->p2) / 2));
}
/****************************************************************************************
* Name: Report()
* Purpose: to output the type of shape and coordinates inside the line object.
* Function In parameters: none
* Function Out Parameters: returns void
* Version: 1.0
* Author: Robert Taracha
****************************************************************************************/
void Line::Report(){
	cout << "Shape LINE " << this->name << endl;
	cout << "start coordinates: ";
	p1.Report(); 
	cout << "end coordinates: ";
	p2.Report();

}