/****************************************************************************************
* Filename: VectorGraphic.cpp
* Version: 1.0
* Author: Robert Taracha
* Student No: 040820843
* Course Name/Number: C++ CST8219
* Lab Sect: 302
* Assignment #: 3
* Assignment Name: Vector Graphic
* Prof. Andrew Tyler
* Due Date : 12/2/16
* Submitted : 12/2/16
* Header Files: VectorGraphic.h, Ellipse.h, Line.h, Rectangle.h
* Purpose: The user expands and reduces a array of vector graphic elements, each of which are clearly defiened.
			VectorGraphic uses the vector STL, inorder to operate the array.
*****************************************************************************************/
#include<iostream>
#include "Line.h"
#include "Rectangle.h"
#include "Ellipse.h"
#include"VectorGraphic.h"

using namespace std;

VectorGraphic::VectorGraphic(){}
/****************************************************************************************
* Name: ~VectorGraphic()
* Purpose: This destructor is responsible for freeing the allocated shape memory
* Function In parameters: none
* Function Out Parameters: returns void
* Version: 1.0
* Author: Robert Taracha
****************************************************************************************/
VectorGraphic::~VectorGraphic(){
	VectorGraphic::iterator it;
	for (it = begin(); it != end(); it++){
		GraphicElement::iterator gt;
		for (gt = (*it).begin(); gt != (*it).end(); gt++){
			delete *gt;
		}
	}
}
/****************************************************************************************
* Name: AddGraphicElement()
* Purpose: This function adds a new Graphic Element to the VectorGraphic vector array list.
* Function In parameters: none
* Function Out Parameters: returns void
* Version: 1.0
* Author: Robert Taracha
****************************************************************************************/
void VectorGraphic::AddGraphicElement() {
	double x_1, y_1, x_2, y_2;
	char nameOfShape[256];
	char name[256]; 
	unsigned int numShapes = 0;
	char type;
	Shape ** shapes;
	cout << "Adding a Graphic Element" << endl;
	cout << "Please enter the name of the new Graphic Element (<256 characters): ";
	cin.ignore();
	cin.getline(name, 256);
	cout << "Please enter the number of Shapes it contains: ";
	cin >> numShapes;
	shapes = new Shape*[numShapes];

	for (int j = 0; j < numShapes; j++){
		cout << "Please enter the type( L for line, R for rectangle, E for ellipse) for Shape #" << j << endl;
		cin >> type;
		type = toupper(type);
		if (type == 'L'){
			cout << "Please enter the name of the new line(<256 characters): ";
			cin.ignore();
			cin.getline(nameOfShape, 256);
			cout << "please enter the coordinates of the start point: (x,y) ";
			cin >> x_1 >> y_1;
			cout << "please enter the coordinates of the end point: (x,y) ";
			cin >> x_2 >> y_2;
			Pair start = Pair(x_1, y_1);
			Pair end = Pair(x_2, y_2);
			shapes[j] = new Line(nameOfShape, start, end);

		}
		if (type == 'R'){
			cout << "Please enter the name of the new Rectangle(<256 characters): ";
			cin.ignore();
			cin.getline(nameOfShape, 256);
			cout << "please enter the coordinates of the top-left: (x,y)";
			cin >> x_1 >> y_1;
			cout << "please enter the coordinates of the bottom-right: (x,y) ";
			cin >> x_2 >> y_2;
			Pair topLeft = Pair(x_1, y_1);
			Pair bottomRight = Pair(x_2, y_2);
			shapes[j] = new Rectangle(nameOfShape, topLeft, bottomRight);

		}
		if (type == 'E'){
			cout << "Please enter the name of the new Ellipse(<256 characters): ";
			cin.ignore();
			cin.getline(nameOfShape, 256);
			cout << "please enter the coordinates of the centre: (x,y) ";
			cin >> x_1 >> y_1;
			cout << "please enter the width and height: (width, height) ";
			cin >> x_2 >> y_2;
			Pair centre = Pair(x_1, y_1);
			Pair axes = Pair(x_2, y_2);
			shapes[j] = new Ellipse(nameOfShape, centre, axes);
		}
	}
	push_back(GraphicElement(shapes, name, numShapes));

	delete[] shapes;
}
/****************************************************************************************
* Name: DeleteGraphicElement()
* Purpose: This function removes a selected Graphic Element from the VectorGraphic vector array
* Function In parameters: none
* Function Out Parameters: returns void
* Version: 1.0
* Author: Robert Taracha
****************************************************************************************/
void VectorGraphic::DeleteGraphicElement() {
	int selected = -1;
	while ((selected > (size())) && selected < 0){
		cout << "Please enter the index to delete in the range 0 to " << (size() - 1)<<endl;
		cin >> selected;
	}
	cout << "erase index = " << selected << endl;
	GraphicElement::iterator gt;
	for (gt = (this->at(selected).begin()); gt != (this->at(selected)).end(); gt++){
			delete *gt;
	}
	this->erase(this->begin() + selected);
}
/****************************************************************************************
* Name: operator<<
* Purpose: operator overloaded to print into the output stream each GraphicElement.
* Function In parameters: none
* Function Out Parameters: returns void
* Version: 1.0
* Author: Robert Taracha
****************************************************************************************/
ostream& operator<<(ostream& os, VectorGraphic& vc){
	cout << " Vector Graphic Report";
	int tracker = 0;
	VectorGraphic::iterator it;
	for (it = vc.begin(); it != vc.end(); it++){
		cout << "Reporting Graphic Element " << tracker << endl;
		cout << *it << endl;
		tracker++;
	}
	
	return cout;
}
