/****************************************************************************************
* Filename: GraphicElement.cpp
* Version: 1.0
* Author: Robert Taracha
* Student No: 040820843
* Course Name/Number: C++ CST8219
* Lab Sect: 302
* Assignment #: 3
* Assignment Name: Vector Graphic
* Prof. Andrew Tyler
* Due Date : 12/2/16
* Submitted : 12/2/16
* Header Files:  GraphicElement.h, iostream, string
* Purpose: Controlls the GraphicElement array, adds each shape into the vector. 
*****************************************************************************************/
#include <iostream>
#include <string>
#include "GraphicElement.h"
#define _CRT_SECURE_NO_WARNINGS
using namespace std;
/****************************************************************************************
* Name: GraphicElement()
* Purpose: A constructor with set parameters which are used to create the vector array.
* Function In parameters: Shape **, char*, unsigned int
* Version: 1.0
* Author: Robert Taracha
****************************************************************************************/
GraphicElement::GraphicElement(Shape** s, char* n, unsigned int numshapes){
	//add each shape to the vector arry of shapes.

	strcpy_s(name, n); // copy over the name of the element.

	for (int j = 0; j < numshapes; j++)
	{	
		push_back(s[j]);
	}
	
}
/****************************************************************************************
* Name: GraphicElement()
* Purpose: Copy Construcotr, makes a exact copy of a GraphicElement object
* Function In parameters: cons GraphicElement&
* Function Out Parameters: returns void
* Version: 1.0
* Author: Robert Taracha
****************************************************************************************/
GraphicElement::GraphicElement(const GraphicElement& gElement){
	strcpy_s(name, gElement.name);
	GraphicElement::const_iterator it;
	for (it = gElement.begin(); it != gElement.end(); it++)
	{
		push_back(*it);
	}

}
GraphicElement::~GraphicElement(){}
GraphicElement& GraphicElement::operator=(GraphicElement& gElement){
	clear();
	strcpy_s(name, gElement.name);
	GraphicElement::const_iterator it;
	for (it = gElement.begin(); it != gElement.end(); it++)
	{
		push_back(*it);
	}
	return *this;
}
/****************************************************************************************
* Name: operator<<
* Purpose: operator overloaded call report fucntion on each of the shapes, and places it into the output stream.
* Function In parameters: none
* Function Out Parameters: returns void
* Version: 1.0
* Author: Robert Taracha
****************************************************************************************/
ostream& operator<<(ostream& os, GraphicElement& gGraphic){
	Pair centre = Pair(0,0);
	double count = 0;
	cout << endl;
	cout << "Graphic Element " << gGraphic.name << endl;
	cout << "The centre =";
	GraphicElement::iterator gt;
	for (gt = gGraphic.begin(); gt != gGraphic.end(); gt++)
	{
		centre = centre + (*gt)->CalculateCentre();
		count++;
	}
	centre = centre / count;
	centre.Report();
	GraphicElement::iterator it;
	for (it = gGraphic.begin(); it != gGraphic.end(); it++)
	{
		(*it)->Report();
	}
	return cout;
}