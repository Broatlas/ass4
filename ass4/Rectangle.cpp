/****************************************************************************************
* Filename: Rectangle.cpp
* Version: 1.0
* Author: Robert Taracha
* Student No: 040820843
* Course Name/Number: C++ CST8219
* Lab Sect: 302
* Assignment #: 3
* Assignment Name: Vector Graphic
* Prof. Andrew Tyler
* Due Date : 12/2/16
* Submitted : 12/2/16
* Header Files:  Rectangle.h, iostream
* Purpose: Defines output for Line classs
*****************************************************************************************/
#include <iostream>
#include "Rectangle.h"
using namespace std;
/****************************************************************************************
* Name: CalculateCentre()
* Purpose: to return the centre coordinates of the rectangle shape.
* Function In parameters: none
* Function Out Parameters: returns Pair
* Version: 1.0
* Author: Robert Taracha
****************************************************************************************/
Pair Rectangle::CalculateCentre(){
	return Pair((this->p1 + this->p2)/2);
}
/****************************************************************************************
* Name: Report()
* Purpose: to output the type of shape and coordinates inside the rectangle object.
* Function In parameters: none
* Function Out Parameters: returns void
* Version: 1.0
* Author: Robert Taracha
****************************************************************************************/
void Rectangle::Report(){
	cout << "Shape RECTANGLE " << this->name << endl;
	cout << "top left coordinates: ";
	this->p1.Report();
	cout << "bottom left coordinates: ";
	this->p2.Report(); 
}