// GraphicElement.h
#ifndef GRAPHICELEMENT_H_
#define GRAPHICELEMENT_H_
#include"Shape.h"
#include <vector>
using namespace std;

class GraphicElement : public vector<Shape*>
{
	static const int SIZE = 256;
	char name[SIZE];
public:
	GraphicElement(Shape**, char*, unsigned int);
	GraphicElement(const GraphicElement&);
	GraphicElement& operator=(GraphicElement&);
	~GraphicElement();
	friend ostream& operator<<(ostream&, GraphicElement&);
};
#endif