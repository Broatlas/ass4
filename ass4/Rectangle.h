// Rectangle.h
#ifndef RECTANGLE_H_
#define RECTANGLE_H_
#include "Shape.h"
class Rectangle :public Shape
{
public:
	Rectangle(char* name, Pair topLeft, Pair bottomRight) :Shape(name, topLeft, bottomRight){};
	~Rectangle(){}
	Pair CalculateCentre();
	void Report();
};
#endif

