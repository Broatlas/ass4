
// Ellipse.h
#ifndef ELLIPSE_H_
#define ELLIPSE_H_
#include "Shape.h"
class Ellipse :public Shape
{
public:

	Ellipse::Ellipse(char* name, Pair centre, Pair axes) :Shape(name, centre, axes){}
	~Ellipse(){}
	Pair CalculateCentre();
	void Report();
};
#endif
