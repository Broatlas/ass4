/****************************************************************************************
* Filename: Pair.cpp
* Version: 1.0
* Author: Robert Taracha
* Student No: 040820843
* Course Name/Number: C++ CST8219
* Lab Sect: 302
* Assignment #: 3
* Assignment Name: Vector Graphic
* Prof. Andrew Tyler
* Due Date : 12/2/16
* Submitted : 12/2/16
* Header Files:  Pair.h, iostream
* Purpose: Defines the pair object, which is the used as the coordinates for shape objects.
*****************************************************************************************/
#include <iostream>
#include "Pair.h"

using namespace std;
/****************************************************************************************
* Name: Report()
* Purpose: Reports the coordinate pair of this object.
* Function In parameters: none
* Function Out Parameters: returns void
* Version: 1.0
* Author: Robert Taracha
****************************************************************************************/
void Pair::Report(){
	cout << " x = " << x << "; y = " << y << endl;
}
/****************************************************************************************
* Name: operator+
* Purpose: operator overloaded to add the pair objects x and y coordinates together and return to a new pair object.
* Function In parameters: Pair& 
* Function Out Parameters: returns Pair
* Version: 1.0
* Author: Robert Taracha
****************************************************************************************/
Pair Pair::operator+(Pair& p){
	double x_add, y_add;
	x_add = x + p.x;
	y_add = y + p.y;
	return Pair(x_add, y_add);
}
/****************************************************************************************
* Name: operator/
* Purpose: operator overloaded to use a single double argument and divide both x and y coordinates of the Pair object
			and return a new pair with the new coordinates.
* Function In parameters: double
* Function Out Parameters: returns Pair
* Version: 1.0
* Author: Robert Taracha
****************************************************************************************/
Pair Pair::operator/(double x){

	double x_div, y_div;

	x_div = this->x / x;
	y_div = this->y / x;

	return Pair(x_div, y_div);
}