// Pair.h
#ifndef  PAIR_H_
#define PAIR_H_
class Pair
{
	double x, y;
public:
	Pair() :x(0), y(0){}
	Pair(double x, double y) :x(x), y(y){}
	Pair operator+(Pair&);
	Pair operator/(double);
	void Report();
};
#endif
