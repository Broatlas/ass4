// VectorGraphic.h
#ifndef VECTORGRAPHIC_H_
#define VECTORGRAPHIC_H_
#include "GraphicElement.h" 
class VectorGraphic : public vector<GraphicElement>
{
public:
	VectorGraphic();
	~VectorGraphic();
	void AddGraphicElement();
	void DeleteGraphicElement();
	friend ostream& operator<<(ostream&, VectorGraphic&);
};
#endif
